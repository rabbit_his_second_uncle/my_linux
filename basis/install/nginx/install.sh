#! /usr/bin/bash
source ../start.sh
version=1.14.0
dependencies() {
  yum -y install pcre pcre-devel
}

download() {
  nginx_tgz=nginx-$version.tar.gz
	if [ ! -f $download/$nginx_tgz ];then
		wget http://nginx.org/download/$nginx_tgz
		tar zxvf $nginx_tgz 
	fi
}

install() {
	cd $download/nginx-$version
  if [ ! -d $prefix/nginx ]; then
    mkdir -p $prefix/nginx
  fi
	./configure --prefix=${prefix}/nginx --user=www --group=www  --with-http_stub_status_module --with-http_ssl_module
	make;make install
}

usergroup() {
	groupadd www 
	useradd -g www www 
}

config() {
  cp $root/nginx.conf $prefix/nginx/conf
  cp $root/sites.conf $prefix/nginx/conf
  cp $root/NginxLogCut.sh $prefix/nginx/sbin/
  mkdir -p /logs/nginx
  chown -R www:www /logs/nginx
  cp $root/nginx.service $sysctl_dir
  systemctl restart nginx
  systemctl enable nginx
  
}

restart() {
	systemctl restart nginx
}

check() {
  echo "temp no"
}

run $1
