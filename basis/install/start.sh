#! /usr/bin/bash
root=$PWD
download=~/Downloads
prefix=/usr/local/cellar
sysctl_dir=/usr/lib/systemd/system/


run() {
	if [ ! -d $download ];then
	  mkdir $download
	fi
	cd $download
	if [ x$1 = "xinstall" ];then
	  dependencies
	  download
	  usergroup
	  install
	  config
	  restart
	  check
	fi
}