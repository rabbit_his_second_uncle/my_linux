#! /usr/bin/bash
source ../start.sh
version=2.24.0
dependencies() {
	yum -y remove git
  	yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel
	yum -y install perl-ExtUtils-MakeMaker package
	yum -y install gcc gcc-c++ libstdc++-devel

}

download() {
  	git_tgz=git-$version.tar.gz
	if [ ! -f $download/$git_tgz ];then
		wget https://mirrors.edge.kernel.org/pub/software/scm/git/$git_tgz
		tar zxvf $git_tgz 
	fi
}


install() {
	if [ ! -d $prefix/git ]; then
	  mkdir -p $prefix/git
	fi
	cd $download/git-$version
	make prefix=$prefix/git all
	make prefix=$prefix/git install
	ln -s $prefix/git/bin/git /usr/bin/git

}

usergroup() {
	echo "no need;"
}

config() {
	echo "no need;"
}

restart() {
	echo "no need;"
}

check() {
	git --version
}

run $1
